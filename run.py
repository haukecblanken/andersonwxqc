from os import makedirs, path
import pickle

from qc_functions import *

print('Loading data...')
#load config file, and read VDR and weather station data
cfg = IO().load_parameters()
vdata = IO().read_vdr(cfg)
wxdata = IO().load_wx_data(cfg)

print('Making output directories...')
#create required directories
makedirs(cfg['output_folder'], exist_ok=True)
makedirs(path.join(cfg['output_folder'],'qc_logs'), exist_ok=True)

print('Processing wind record...')
#derive true wind velocity from apparent wind velocity and vessel motion
wxdata = Process().process_wx_data(cfg, wxdata, vdata)

print('Downsampling data...')
#downsample weather data to sepcified interval
wx_downsampled = Process().downsample_data(wxdata, 60., cfg['downsampling_interval'])

print('Pickling full data...')
with open(path.join(cfg['output_folder'], 'qc_logs', 'data.pickle'), 'wb', -1) as fid:
    pickle.dump(wxdata, fid)

with open(path.join(cfg['output_folder'], 'qc_logs', 'data_downsampled.pickle'), 'wb', -1) as fid:
    pickle.dump(wx_downsampled, fid)

print('Writing output...')
#write data to output files
IO().write_data_to_csv(path.join(cfg['output_folder'], 'data_native_frequency.csv'), wxdata)
IO().write_data_to_csv(path.join(cfg['output_folder'], 'data_downsampled.csv'), wx_downsampled)

print('Writing diagnostics...')
#make summary plots
output().summary_plot(cfg, wxdata, fname=path.join(cfg['output_folder'], 'summary_plot.png'))
output().summary_plot(cfg, wx_downsampled, fname=path.join(cfg['output_folder'], 'summary_plot_downsampled.png'), freq=(1./cfg['downsampling_interval']))

#output diagnostics
output().ranges(cfg, wxdata, fname='variable_ranges_native')
output().ranges(cfg, wx_downsampled, fname='variable_ranges_downsampled')
output().correlations_native_downsampled(cfg, wxdata, wx_downsampled)
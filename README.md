# AndersonWxQC

Small project to process weather station data aboard *R/V Doug Anderson* and derive true wind velocities from recorded 
apparent wind velocities and vessel NMEA data logged using OpenCPN. 

Procedure for use is as follows:
1. fill in parameters.yaml in accordance with the annotations contained in this file
2. from the project home directory run `python run.py`

The code will create the specified output folder and save the processed data within it at both the native data frequency
and the specified downsampled frequency. It will also create summary plots of both data sets, and several diagnostic logs 
in the subfolder qc_logs.

The code expects that weather station data is collected at one minute resolution.

Note that the timestamps and vessel coordinates in the output files refer to the end of the averaging period rather than 
coordinates centered on the averaging period. Therefore it is not advisable to choose long downsampling intervals
(ten minutes or longer) as vessel position, and therefore location of the measurement, becomes ill-defined.

TODO: add program file for Campbell Scientific CR300 data logger in order to ensure compatible weather station output.
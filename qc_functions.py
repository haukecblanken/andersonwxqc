import numpy as np
import glob
import datetime
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from scipy import signal
import yaml
from os import path, makedirs


class utils:
    def parse_time(self, time):
        return datetime.datetime.strptime(time, '%Y-%m-%dT%H:%M:%S')

    def between(self, vector, t1, t2):
        return np.where(np.logical_and(vector >= t1, vector <= t2))[0]

    def average_heading(self, vector, threshold=60.):
        if np.nanstd(vector) > threshold:
            vector[vector < 0.] += 360.
        output = np.nanmean(vector)
        if output > 180.:
            output -= 360.

        return output

    def vector_components(self, spd, direc):
        u = spd * np.sin(direc * np.pi / 180.)
        v = spd * np.cos(direc * np.pi / 180.)

        return u, v

    def simple_spec(self, var, freq):
        var[np.isnan(var)] = 0.
        var = var - var.mean()
        f, P = signal.welch(var, fs=freq, nfft=256)

        return f, P


class IO:
    def load_parameters(self):
        with open('./parameters.yaml') as fid:
            cfg = yaml.safe_load(fid)

        return cfg

    def load_vdr_data(self, cfg):
        files = sorted(glob.glob(path.join(cfg['vdr_folder'], '*')))

        lines = []
        for f in files:
            with open(f, 'r') as fid:
                lines += [line[:-2] for line in fid]

        rmc = [l for l in lines if l.startswith('$GPRMC')]
        head = [l for l in lines if l.startswith('$HCHDG')]

        return rmc, head

    def parse_vdr(self, rmc, head):
        vdata = {'time': [], 'lat': [], 'lon': [], 'cog': [], 'sog': [], 'mhd': [], 'mdev': [], 'thd': []}

        for l, ll in zip(rmc, head):
            if len(l.split(',')) != 14:
                continue
            t = l.split(',')[1]
            hours = float(t[:2])
            minutes = float(t[2:4])
            seconds = float(t[4:])
            d = l.split(',')[9]
            day = int(d[:2])
            month = int(d[2:4])
            year = 2000 + int(d[4:])

            dt = hours * 3600. + minutes * 60. + seconds
            vdata['time'].append(datetime.datetime(year, month, day, 0, 0, 0) + datetime.timedelta(seconds=dt))

            vdata['lat'].append(float(l.split(',')[3][:2]) + float(l.split(',')[3][2:]) / 60.)
            vdata['lon'].append(-(float(l.split(',')[5][:3]) + float(l.split(',')[5][3:]) / 60.))

            vdata['sog'].append(float(l.split(',')[7]))
            try:
                vdata['cog'].append(float(l.split(',')[8]))
            except:
                vdata['cog'].append(np.nan)
            vdata['mdev'].append(float(l.split(',')[10]))

            vdata['mhd'].append(float(ll.split(',')[1]))
            vdata['thd'].append(vdata['mhd'][-1] + vdata['mdev'][-1])

        for vk in vdata.keys():
            vdata[vk] = np.asarray(vdata[vk])

        # ensure all headings are less than 360
        vdata['thd'][vdata['thd'] >= 360.] -= 360.

        # convert COG and heading from 0->360 to -180->180
        vdata['thd'][vdata['thd'] > 180.] -= 360.
        vdata['cog'][vdata['cog'] > 180.] -= 360.

        # convert speed from knots to m/s
        vdata['sog'] = vdata['sog'] * 0.514444

        return vdata

    def read_vdr(self, cfg):
        rmc, head = self.load_vdr_data(cfg)
        vdata = self.parse_vdr(rmc, head)

        return vdata

    def load_wx_data(self, cfg):
        with open(cfg['wx_file'], 'r') as fid:
            lines = [line for line in fid]

        wtime = np.array([datetime.datetime.strptime(l.split(',')[0], '"%Y-%m-%d %H:%M:%S"') for l in lines[4:]])
        wxdata = {'time': wtime}
        for l in lines[4:]:
            la = l.split(',')
            for key, num in zip(['solar', 'precip', 'spd', 'direc', 'T', 'P', 'q'], [4, 5, 6, 7, 9, 10, 11]):
                if key not in wxdata.keys():
                    wxdata[key] = []
                try:
                    val = float(la[num])
                except:
                    val = np.nan
                wxdata[key].append(val)

        c = utils().between(wxdata['time'], utils().parse_time(cfg['wx_times']['start']), utils().parse_time(cfg['wx_times']['end']))
        wxdata['time'] = wxdata['time'][c]
        for key in ['solar', 'precip', 'spd', 'direc', 'T', 'P', 'q']:
            wxdata[key] = np.asarray(wxdata[key])[c]
            if key == 'precip':
                wxdata[key] = wxdata[key] / (1. / 60.)

        # convert wind direction from 'blowing from' to 'blowing towards'
        wxdata['direc'] += 180.
        wxdata['direc'][wxdata['direc'] >= 360.] -= 360.

        # convert wind direction from 0->360 to -180->180
        wxdata['direc'][wxdata['direc'] > 180.] -= 360.

        # implement thresholds of acceptable values
        for k in cfg['wx_thresholds'].keys():
            c = np.where(np.logical_or(wxdata[k] < cfg['wx_thresholds'][k]['min'],
                                       wxdata[k] > cfg['wx_thresholds'][k]['max']))[0]
            wxdata[k][c] = np.nan

        return wxdata

    def write_data_to_csv(self, fname, data):
        header_lines = [
            'Time, Latitude, Longitude, Wind Speed, Wind Direction, E/W Wind Speed, N/S Wind Speed, \\'
            'Atmospheric Pressure, Air Temperature, Solar Radiation, Precipitation Rate, Relative Humidity\n',
            '(UTC), (degree), (degree), (m/s), (degT - blowing from), (m/s), (m/s), (mbar), (Celsius), (W/m2), \\'
            '(mm/hr), (percent)\n']

        with open(fname, 'w') as fid:
            #write header lines
            for h in header_lines:
                fid.write(h)
            #empty line between header and data
            fid.write('\n')

            #write data line by line
            for i in range(len(data['time'])):
                wstr = datetime.datetime.strftime(data['time'][i], '%Y/%m/%d %H:%M:%S')
                for fmat, var in zip(['%.4f', '%.4f', '%.2f', '%.1f', '%.2f', '%.2f', '%.1f', '%.2f', '%.1f', '%.1f', '%.1f'],
                                     ['lat', 'lon', 'spd_corrected', 'direc_corrected', 'uw', 'vw', 'P', 'T', 'solar', 'precip', 'q']):
                    wstr += ', ' + fmat % data[var][i]
                fid.write(wstr + '\n')


class Process:
    def get_vdr_times(self, cfg, vdata, hrcut=8.):
        start_times = [vdata['time'][0]]
        end_times = []

        for i in range(1, len(vdata['time']) - 1):
            if (vdata['time'][i] - vdata['time'][i - 1]).total_seconds() >= hrcut * 3600.:
                start_times.append(vdata['time'][i])
            if (vdata['time'][i + 1] - vdata['time'][i]).total_seconds() >= hrcut * 3600.:
                end_times.append(vdata['time'][i])
        end_times.append(vdata['time'][-1])

        with open(path.join(cfg['output_folder'], 'qc_logs', 'vdr_active_times.txt'), 'w') as fid:
            for st, et in zip(start_times, end_times):
                line = 'Start: %s, End: %s\n' % (datetime.datetime.strftime(st, '%Y/%m/%d %H:%M:%S'),
                                                 datetime.datetime.strftime(et, '%Y/%m/%d %H:%M:%S'))
                fid.write(line)

        return start_times, end_times

    def get_equipment_status(self, cfg, vdata, wxdata):
        start_times, end_times = self.get_vdr_times(cfg, vdata)
        time_vector = np.array([t.item() for t in np.arange(wxdata['time'][0],
                                                            wxdata['time'][-1] + datetime.timedelta(seconds=60.),
                                                            datetime.timedelta(seconds=60.))])

        vdr_active = np.zeros([len(time_vector), ]).astype(bool)
        docked = np.zeros([len(time_vector), ]).astype(bool)

        wx_on = np.array([t in wxdata['time'] for t in time_vector])

        for st, et in zip(start_times, end_times):
            c = utils().between(time_vector, st, et)
            vdr_active[c] = True

        for st, et in zip(end_times[:-1], start_times[1:]):
            c = np.where(np.logical_and(vdr_active == False, np.logical_and(time_vector >= st, time_vector <= et)))[0]
            docked[c] = True

        for gap_type, target in zip(['ops_wo_vdr', 'data_download'], [docked, wx_on]):
            if len(cfg['data_gaps'][gap_type]) > 0:
                for pair in cfg['data_gaps'][gap_type]:
                    c = utils().between(time_vector, utils().parse_time(pair['start']), utils().parse_time(pair['end']))
                    target[c] = False

        return time_vector, {'vdr': vdr_active, 'docked': docked, 'wx':wx_on}

    def initialize_new_values(self, wxdata):
        wxdata['spd_corrected'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['direc_corrected'] = np.zeros_like(wxdata['direc']) * np.nan

        wxdata['u_raw'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['v_raw'] = np.zeros_like(wxdata['direc']) * np.nan

        wxdata['u_vessel'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['v_vessel'] = np.zeros_like(wxdata['direc']) * np.nan
        wxdata['cog'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['sog'] = np.zeros_like(wxdata['direc']) * np.nan

        wxdata['uw'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['vw'] = np.zeros_like(wxdata['direc']) * np.nan

        wxdata['lat'] = np.zeros_like(wxdata['spd']) * np.nan
        wxdata['lon'] = np.zeros_like(wxdata['direc']) * np.nan

        return wxdata

    def process_wx_data(self, cfg, wxdata, vdata, epoch=datetime.datetime(1970, 1, 1, 0, 0, 0)):
        time_vector, status = self.get_equipment_status(cfg, vdata, wxdata)

        wxdata = self.initialize_new_values(wxdata)

        for i, t in enumerate(time_vector):
            if not status['wx'][i]:
                continue

            cw = np.where(wxdata['time'] == t)[0][0]
            if not status['vdr'][i]:
                if status['docked'][i]:
                    #add heading in dock to recorded wind direction
                    wxdata['direc_corrected'][cw] = cfg['docked_heading'] + wxdata['direc'][cw]

                    #ensure sum conforms to -180->180 convention
                    if wxdata['direc_corrected'][cw] > 180.:
                        wxdata['direc_corrected'][cw] -= 360.
                    if wxdata['direc_corrected'][cw] < -180.:
                        wxdata['direc_corrected'][cw] += 360.

                    #wind speed is accurate as recorded when docked
                    wxdata['spd_corrected'][cw] = wxdata['spd'][cw]

                    #compute vector components
                    wxdata['uw'][cw], wxdata['vw'][cw] = utils().vector_components(wxdata['spd_corrected'][cw],
                                                                                 wxdata['direc_corrected'][cw])
                else:
                    continue
            else:
                #determine averaging data for VDR data (2 secodn interval) to correspond with wind record entry (60 second interval)
                c_vdr_time = utils().between(vdata['time'], t+datetime.timedelta(seconds=-60), t)
                c_cog_valid = np.where(np.isfinite(vdata['cog']))[0]
                c_vdr_avg = np.intersect1d(c_vdr_time, c_cog_valid)

                #do not proceed if there is no VDR data
                if len(c_vdr_avg) == 0:
                    continue

                #record average latitude and longitude over weather record period
                wxdata['lat'][cw] = np.nanmean(vdata['lat'][c_vdr_avg])
                wxdata['lon'][cw] = np.nanmean(vdata['lon'][c_vdr_avg])

                #get the average heading over the weather record period
                heading = utils().average_heading(vdata['thd'][c_vdr_avg])
                #add average heading to recorded wind direction
                direc_corr = heading + wxdata['direc'][cw]
                #compute recorded wind velocity components based on corrected wind direction
                wxdata['u_raw'][cw], wxdata['v_raw'][cw] = utils().vector_components(wxdata['spd'][cw], direc_corr)

                #average vessel speed and course over ground for the weather record period
                sog = np.nanmean(vdata['sog'][c_vdr_avg])
                cog = utils().average_heading(vdata['cog'][c_vdr_avg])
                #set vessel velocity components to zero when speed over ground is zero
                #if not, compute the vessel velocity components
                if np.logical_or(sog == 0., np.isnan(cog)):
                    wxdata['u_vessel'][cw] = 0.
                    wxdata['v_vessel'][cw] = 0.
                else:
                    wxdata['u_vessel'][cw], wxdata['v_vessel'][cw] = utils().vector_components(sog, cog)
                #record speed and course over ground for troubleshooting availability
                wxdata['sog'][cw] = sog
                wxdata['cog'][cw] = cog

                #add apparent wind and vessel velocity components to derive true wind velocity
                #Note: components are added since vessel velocity results in an apparent wind velocity
                #in the opposite direction
                wxdata['uw'][cw] = wxdata['u_raw'][cw] + wxdata['u_vessel'][cw]
                wxdata['vw'][cw] = wxdata['v_raw'][cw] + wxdata['v_vessel'][cw]

                #compute corrected wind speed and direction from velocity components
                wxdata['spd_corrected'][cw] = np.sqrt(wxdata['uw'][cw] ** 2. + wxdata['vw'][cw] ** 2.)
                wxdata['direc_corrected'][cw] = np.arctan2(wxdata['uw'][cw], wxdata['vw'][cw]) * 180. / np.pi

        # convert wind direction back to 'blowing from, 0->360'
        wxdata['direc_corrected'] += 180.
        wxdata['direc_corrected'][wxdata['direc_corrected'] > 180.] -= 360.
        wxdata['direc_corrected'][wxdata['direc_corrected'] < 0.] += 360.

        return wxdata

    def downsample_data(self, data, timestep, interval):
        #make sure downsampling interval is a multiple of original interval
        if interval % timestep != 0.:
            raise ValueError('Downsampling interval must be an multiple of the time step.')

        #check if there are any gaps in the data, and if so fill them with NaN to accommodate index-wise downsampling
        dt_check = np.array([dt.total_seconds() for dt in np.diff(data['time'])])
        if len(np.unique(dt_check)) > 1:
            time_vector = np.array([t.item() for t in np.arange(data['time'][0], data['time'][-1],
                                                                datetime.timedelta(seconds=timestep))])
            arr = {'time': time_vector}
            for k in data.keys():
                if k != 'time':
                    tmp = np.zeros([len(time_vector), ]) * np.nan
                    for i in range(len(tmp)):
                        if time_vector[i] in data['time']:
                            c = np.where(data['time'] == time_vector[i])[0][0]
                            tmp[i] = data[k][c]
                        else:
                            continue
                    arr[k] = tmp
                else:
                    continue
        else:
            arr = data

        #downsample data through arithmetic averaging
        N = int(interval / timestep)
        avg = {k: np.array([np.nanmean(arr[k][i * N:(i + 1) * N]) for i in range(len(time_vector[::N]))]) for k in
               arr.keys() if k != 'time'}
        avg['time'] = time_vector[::N]

        #delete any remaining NaNs from the output
        nan_check = np.array(
            [np.isnan(np.array([avg[k][i] for k in arr.keys() if k != 'time'])).all() for i in range(len(avg['time']))])
        avg = {k: np.delete(avg[k], np.where(nan_check)) for k in avg.keys()}

        return avg

class output:
    def summary_plot(self, cfg, data, fname='./summary_plot.png', quiver_flag=False, freq=(1. / 60.)):
        fig = plt.figure(figsize=(7.5, 10))
        gs = GridSpec(4, 2)
        ax = fig.add_subplot(gs[0, :])
        if quiver_flag:
            qv = ax.quiver(data['time'], np.zeros([len(data['time']), ]), data['uw'], data['vw'], scale=100, color='k')
            plt.quiverkey(qv, 0.94, 0.94, 5., '5 m/s', labelpos='S', coordinates='axes')
            ax.set(yticks=[], yticklabels=[])
        else:
            ax.plot(data['time'], data['uw'], color='r', alpha=0.5, ls='-', label='E/W component')
            ax.plot(data['time'], data['vw'], color='b', alpha=0.5, ls='dotted', label='N/S component')
            ax.set_ylabel('m/s')
            ax.legend()
        start_time = utils().parse_time(cfg['wx_times']['start'])
        end_time = utils().parse_time(cfg['wx_times']['end'])
        xticks = [datetime.datetime(start_time.year,start_time.month,start_time.day,0,0,0) + datetime.timedelta(days=i) for i in range(end_time.day - start_time.day + 2)]
        ax.set_xticks(xticks)
        ax.set_xticklabels([datetime.datetime.strftime(t, '%Y/%m/%d') for t in xticks], rotation=20.)
        ax.text(0.02, 0.98, 'Wind Velocity', ha='left', va='top', transform=ax.transAxes)

        ax = fig.add_subplot(gs[3, 0])
        ax.plot(data['time'], data['P'], color='k', alpha=0.6)
        ax.set(xticks=xticks)
        ax.set_xticklabels([datetime.datetime.strftime(t, '%Y/%m/%d') for t in xticks], rotation=30.)
        ax.text(0.02, 0.98, 'Barometric\nPressure', ha='left', va='top', transform=ax.transAxes)
        ax.set_ylabel('mbar')

        ax = fig.add_subplot(gs[1, 0])
        ax.plot(data['time'], data['T'], color='k', alpha=0.6)
        ax.set(xticklabels=[])
        ax.text(0.02, 0.98, 'Air Temperature', ha='left', va='top', transform=ax.transAxes)
        ax.set_ylabel('degree Celsius')

        ax = fig.add_subplot(gs[2, 0])
        ax.plot(data['time'], data['q'], color='k', alpha=0.6)
        ax.set(xticklabels=[])
        ax.text(0.02, 0.98, 'Relative\nHumidity', ha='left', va='top', transform=ax.transAxes)
        ax.set_ylabel('percent')

        ax = fig.add_subplot(gs[2, 1])
        ax.plot(data['time'], data['precip'], color='k', alpha=0.6)
        ax.set(xticks=xticks)
        ax.set_xticklabels([datetime.datetime.strftime(t, '%Y/%m/%d') for t in xticks], rotation=30.)
        ax.text(0.02, 0.98, 'Precipitation\nRate', ha='left', va='top', transform=ax.transAxes)
        ax.set_ylabel('mm/hr')
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')

        ax = fig.add_subplot(gs[1, 1])
        ax.plot(data['time'], data['solar'], color='k', alpha=0.6)
        ax.set(xticklabels=[])
        ax.text(0.02, 0.98, 'Solar\nRadiation', ha='left', va='top', transform=ax.transAxes)
        ax.set_ylabel('W/m2')
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')

        ax = fig.add_subplot(gs[3, 1])
        fx, Px = utils().simple_spec(data['uw'], freq)
        fy, Py = utils().simple_spec(data['vw'], freq)
        ax.loglog(fx, Px, color='r', alpha=0.5, ls='-', label='E/W component')
        ax.loglog(fy, Py, color='b', alpha=0.5, ls='dotted', label='N/S component')
        ax.set_ylabel(r'$(m/s)^2/Hz$')
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position('right')
        ax.legend(loc=3)
        ax.set_xlabel('Frequency (Hz)')
        ax.text(0.98, 0.98, 'Wind Velocity\nPower\nSpectral\nDensity', ha='right', va='top', transform=ax.transAxes)

        plt.tight_layout()

        plt.savefig(fname, bbox_inches='tight')

    def ranges(self, cfg, data, fname='variable_ranges'):
        with open(path.join(cfg['output_folder'], 'qc_logs', fname + '.csv'), 'w') as fid:
            fid.write('Variable, Minimum, Maximum\n')
            for k in data.keys():
                if k != 'time':
                    fid.write('%s, %.3f, %.3f\n'%(k, np.nanmin(data[k]), np.nanmax(data[k])))

    def correlations_native_downsampled(self, cfg, data, data_downsampled, epoch=datetime.datetime(1970,1,1,0,0,0)):
        with open(path.join(cfg['output_folder'], 'qc_logs', 'downsampling_correlations.csv'), 'w') as fid:
            fid.write('Variable, Correlation Coefficient\n')

            tI = np.array([(t - epoch).total_seconds() for t in data['time']])
            tR = np.array([(t - epoch).total_seconds() for t in data_downsampled['time']])

            for k in data_downsampled.keys():
                if k == 'time':
                    continue

                a = np.interp(tI, tR, data_downsampled[k])
                a[np.isnan(a)] = 0.
                b = data[k]
                b[np.isnan(b)] = 0.
                corr = np.corrcoef(a, b)
                fid.write('%s, %.3f\n'%(k, 0.5 * (corr[0, 1] + corr[1, 0])))
